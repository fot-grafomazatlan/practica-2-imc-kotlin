package com.example.practica2imckotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var txtvAltura: TextView;
    private lateinit var etxtAltura: EditText;
    private lateinit var txtvPeso: TextView;
    private lateinit var etxtPeso: EditText;
    private lateinit var txtvIMC: TextView;
    private lateinit var btnCalcular: Button;
    private lateinit var btnLimpiar: Button;
    private lateinit var btnFinalizar: Button;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtvAltura = findViewById(R.id.txtvAltura);
        etxtAltura = findViewById(R.id.etxtAltura);
        txtvPeso = findViewById(R.id.txtvPeso);
        etxtPeso = findViewById(R.id.etxtPeso);
        txtvIMC = findViewById(R.id.txtvIMC);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnFinalizar = findViewById(R.id.btnFinalizar);


        btnCalcular.setOnClickListener {
            if (etxtAltura.text.toString().isEmpty() ||
                etxtPeso.text.toString().isEmpty()
            ) {
                Toast.makeText(
                    this@MainActivity,
                    "Ingresa los datos correctos",
                    Toast.LENGTH_SHORT
                ).show();
            } else {
                val alt = etxtAltura.text.toString().toDouble();
                val pes = etxtPeso.text.toString().toDouble();
                val res = pes / (alt * alt);
                txtvIMC.text = res.toString();
            }
        }

        btnLimpiar.setOnClickListener {
            etxtAltura.text.clear();
            etxtPeso.text.clear();
            txtvIMC.text =  "";
        }

        btnFinalizar.setOnClickListener {
            finish();
        }
    }
}

